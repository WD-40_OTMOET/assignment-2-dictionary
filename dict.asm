section .text

extern string_equals

global find_word
find_word: ;rsi указывает на начало списка, rdi указывает на искомую строку, возвращает значение в rdi или 0, если слово не найдено
	
.loop:
	test rsi, rsi
	jz .last
	push rsi
	push rdi
;	mov rsi, [rsi]
	lea rsi, [rsi+8]
	call string_equals
	lea r8, [rsi+1]
	pop rdi
	pop rsi
	test al, al
	jnz .find
	mov rsi, [rsi]
	
	jmp .loop
	
.find:
	mov rdi, r8
	jmp .end
	
.last:
	xor rdi, rdi
	
.end:
	ret
	
