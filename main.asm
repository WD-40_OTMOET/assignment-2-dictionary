
section .data
t1: db 'Enter the key to find: ', 0
t2: db 'Value was found: ', 0
t3: db 'Key not found', 0
input: db 256 dup (0)
%include "words.inc"


section .text
extern print_string
extern read_word
extern print_newline
extern find_word

global _start
_start:
    	
    mov rdi, t1
    call print_string
    mov rdi, input
    mov rsi, 256
    call read_word
    mov rdi, input
    mov rsi, head
    
    call find_word
    test rdi, rdi
    jz not_found
    push rdi
    mov rdi, t2
    call print_string
    pop rdi
    call print_string
    jmp p_end
    
    
not_found:
	mov rdi, t3
	call print_string
	
p_end:
call print_newline


    mov rax, 60
    pop rdi
    syscall
