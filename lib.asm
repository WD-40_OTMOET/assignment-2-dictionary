section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
	xor rax, rax
	mov rax, rdi
	xor rdi, rdi
	ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ;
.loop:
    cmp byte [rdi], 0 ;
    jz .exit ;
    inc rax ;
    inc rdi ;
    jmp .loop
.exit: 
    ret 


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
global print_string
print_string:
    xor rax, rax
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi ;
    mov rdx, 1 ;
    mov rsi, rsp ;
    mov rdi, 1 ;
    mov rax, 1 ;
    syscall
    pop rdi ;
    ret

; Переводит строку (выводит символ с кодом 0xA)
global print_newline
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbp
    mov rax, rdi
    mov rcx, 10
    mov rbp, rsp
    dec rsp
    mov byte [rsp], 0
.loop:
    xor rdx, rdx
    div rcx
    dec rsp
    add dl, 0x30
    mov [rsp], dl
    test rax, rax;
    jnz .loop
    mov rdi, rsp
    call print_string
    mov rsp, rbp
    pop rbp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    push rbp
    mov rax, rdi
    xor r8b, r8b
    cmp rax, 0
    jge .conv
    neg rax
    mov r8b, 45
.conv:
    mov rcx, 10
    mov rbp, rsp
    dec rsp
    mov byte [rsp], 0
.loop:
    xor rdx, rdx
    div rcx
    dec rsp
    add dl, 0x30
    mov [rsp], dl
    test rax, rax;
    jnz .loop
    test r8b, r8b
    jz .end
    dec rsp
    mov [rsp], r8b
    
.end:
    mov rdi, rsp
    call print_string
    mov rsp, rbp
    pop rbp
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

global string_equals
string_equals:
	mov rax, 1
	jmp .zero_check1
	
.zero_check1:
	cmp byte [rdi], 0
	jz .zero_check2
	jmp .compare
	
.zero_check2:
	cmp byte [rsi], 0
	jz .end
	xor rax, rax
	jmp .end
	
.compare:
	mov r8b, byte [rdi]
	mov r11b, byte [rsi]
	cmp r8b, r11b
 	jz .loop
 	xor rax, rax
 	jmp .end
 
 .loop:
 	inc rdi
	inc rsi
 	jmp .zero_check1
 	 
.end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
    
read_char:
    push rbp
    mov rbp, rsp
    dec rsp
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .end
    movzx rax, byte [rsp]
    
.end:
    mov rsp, rbp
    pop rbp
    ret 

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

global read_word
read_word:
    mov r8, rdi 
    push rdi
    mov r11, rsi
    dec r11 ; резерв 1 символа под завершение строки 
    push r11
    
    xor rdi, rdi
    mov rdx, 1
    
.loop1:
    mov rsi, r8
    xor rax, rax
    push r11
    syscall
    pop r11
    test rax, rax
    jz .error
    cmp byte [r8], 0x20
    sete r9b
    cmp byte [r8], 0x9
    sete r10b
    or r9b, r10b
    cmp byte [r8], 0xA
    sete r10b
    or r9b, r10b
    test r9b, r9b
    jnz .loop1
    
.loop2:
    inc r8
    sub r11, 1
    jz .error
    mov rsi, r8
    xor rax, rax
    push r11
    syscall
    pop r11
    test rax, rax
    jz .al
    cmp byte [r8], 0x20
    sete r9b
    cmp byte [r8], 0x9
    sete r10b
    or r9b, r10b
    cmp byte [r8], 0xA
    sete r10b
    or r9b, r10b
    test r9b, r9b
    jz .loop2

.al:    
    mov byte [r8], 0
    pop rdx
    sub rdx, r11
    pop rax
    jmp .end
    
.error:
	pop r11
	pop rdi
	xor rax, rax
	xor rdx, rdx
.end:
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
.loop1:
    movzx r8, byte [rdi]
	sub r8b, 48
    cmp r8b, 9
    ja .end
    inc rdx
    inc rdi
    imul rax, 10
	add rax, r8
	jc .error
    jmp .loop1
		
.error:
	xor rdx, rdx
.end:
    ret
 
 

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r9b, byte [rdi]
    cmp r9b, 0x2d
    jne .cont
    inc rdi
    
.cont:
	call parse_uint
	test rdx, rdx
	jz .end
	bt rax, 63
	jc .error
	cmp r9b, 0x2d
	jne .end
	neg rax
	inc rdx
	jmp .end	
	
.error:
	xor rdx, rdx
	
.end:
    ret 


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    mov rcx, rdx
    xor rax, rax
    
.loop:
	test rcx, rcx
	jz .error
    mov dl, byte [rdi]
    mov byte [rsi], dl
    inc rsi
    inc rdi
    test dl, dl
    jz .end
    inc rax
    dec rcx
    jmp .loop
.error:
    xor rax, rax
    
.end:
    ret
