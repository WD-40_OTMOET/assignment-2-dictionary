ASM=nasm
ASMFLAGS=-f elf64
LD=ld

%.o: %.asm
	$(ASM) $(ASMFLAGS) $^


main.o: main.asm words.inc colon.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

main: main.o lib.o dict.o
	$(LD) -o $@ $^

.PHONY: clean
clean:
	rm -f *.o

